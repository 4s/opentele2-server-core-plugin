package org.opentele.server.core
import grails.test.spock.IntegrationSpec
import org.opentele.server.model.Clinician
import org.opentele.server.model.patientquestionnaire.PatientQuestionnaire
import org.opentele.server.model.questionnaire.BooleanNode
import org.opentele.server.model.questionnaire.Questionnaire
import org.opentele.server.model.questionnaire.QuestionnaireHeader
import org.opentele.server.model.questionnaire.QuestionnaireNode

class QuestionnaireServiceIntegrationSpec extends IntegrationSpec {

    QuestionnaireService questionnaireService

    def "can get a list of all active questionnaires with revision"() {
        given:
        def activeQuestionnaires = createQuestionnaires()

        when:
        def found = questionnaireService.allActiveQuestionnaires([:])

        then:
        found.results.size() > 0
        found.totalCount == found.results.size()
        found.results.each { actual ->
            QuestionnaireHeader expected = activeQuestionnaires.find { it.name == actual.name }
            assert expected != null
            assert actual.version == expected.activeQuestionnaire.revision
        }
    }

    def createQuestionnaires() {
        def owner = new Clinician(firstName: "first name", lastName: "last name").save(failOnError: true)
        def activeQuestionnaires = []
        3.times { idx ->
            QuestionnaireHeader questionnaireHeader = new QuestionnaireHeader(name: "TestQuestionnaireHeader"+System.nanoTime()).save()

            def startNode = new BooleanNode(value: true, variableName: "bla", defaultNext: new QuestionnaireNode().save())
            startNode.save(failOnError: true)

            Questionnaire questionnaire = new Questionnaire(questionnaireHeader: questionnaireHeader, revision: "1.$idx", startNode: startNode, creationDate: new Date()).save()
            questionnaire.addToPatientQuestionnaires(new PatientQuestionnaire(templateQuestionnaire: questionnaire, creator: owner, creationDate: new Date()).save())

            questionnaireHeader.addToQuestionnaires(questionnaire)
            questionnaireHeader.setActiveQuestionnaire(questionnaire)

            questionnaireHeader.save(failOnError:true)
            activeQuestionnaires << questionnaireHeader
        }

        activeQuestionnaires
    }

}