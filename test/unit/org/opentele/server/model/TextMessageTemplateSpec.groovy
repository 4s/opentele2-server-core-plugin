package org.opentele.server.model



import grails.test.mixin.*
import grails.validation.ValidationException
import spock.lang.Specification

@TestFor(TextMessageTemplate)
class TextMessageTemplateSpec extends Specification{

    def "name with length up to 255 characters allowed"() {
        given:
        def name = "a".padLeft(255, 'b')

        when:
        new TextMessageTemplate(name: name, content: "bla").save(failOnError: true)

        then:
        notThrown(ValidationException)
    }

    def "content with length up to 1024 characters allowed"() {
        given:
        def content = "a".padLeft(1024, 'b')

        when:
        new TextMessageTemplate(name: "bla", content: content).save(failOnError: true)

        then:
        notThrown(ValidationException)
    }

    def "name cannot be more than 255 characters long"() {
        given:
        def name = "a".padLeft(256, 'b')

        when:
        new TextMessageTemplate(name: name, content: "bla").save(failOnError: true)

        then:
        def ex = thrown(ValidationException)
        ex.errors.hasFieldErrors("name")
        ex.errors.getFieldError("name").defaultMessage.contains("exceeds the maximum size")
    }

    def "content cannot be more than 1024 characters long"() {
        given:
        def content = "a".padLeft(1025, 'b')

        when:
        new TextMessageTemplate(name: "bla", content: content).save(failOnError: true)

        then:
        def ex = thrown(ValidationException)
        ex.errors.hasFieldErrors("content")
        ex.errors.getFieldError("content").defaultMessage.contains("exceeds the maximum size")
    }

    def "can get first part of content"() {
        given:
        def content = "a".padRight(512, 'b')
        def template = new TextMessageTemplate(name: "long message", content: content)

        when:
        def shortContent = template.shortContent()

        then:
        shortContent.size() == 50
        shortContent.startsWith('a')
        template.content.size() > 50
    }

    def "if content is shorter is shorter than first part limit entire content returned"() {
        given:
        def content = "a".padRight(20, 'b')
        def template = new TextMessageTemplate(name: "long message", content: content)

        when:
        def shortContent = template.shortContent()

        then:
        shortContent.size() == 20
        shortContent.startsWith('a')
    }

    def "can get first part of name"() {
        given:
        def name = "a".padRight(128, 'b')
        def template = new TextMessageTemplate(name: name, content: "bla")

        when:
        def shortName = template.shortName()

        then:
        shortName.size() == 20
        shortName.startsWith('a')
        template.name.size() > 20
    }

    def "if name is shorter is shorter than first part limit entire name returned"() {
        given:
        def name = "a".padRight(10, 'b')
        def template = new TextMessageTemplate(name: name, content: "bla")

        when:
        def shortName = template.shortName()

        then:
        shortName.size() == 10
        shortName.startsWith('a')
    }
}
