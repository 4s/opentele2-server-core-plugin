package org.opentele.server.model

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

@TestMixin(GrailsUnitTestMixin)
@Mock(Link)
class LinkSpec extends Specification {

    def "url must be valid url"() {
        given:
        def link = new Link(title: 'bla', url: 'more bla')

        when:
        def isValid = link.validate()

        then:
        isValid == false
        link.errors.errorCount == 1
        link.errors.allErrors[0].field == 'url'
    }
}
