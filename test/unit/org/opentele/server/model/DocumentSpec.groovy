package org.opentele.server.model

import spock.lang.Specification

import grails.test.mixin.*
import grails.test.mixin.support.*

@TestMixin(GrailsUnitTestMixin)
class DocumentSpec extends Specification {

    def "can assign file type from content type string"() {
        when:
        def doc = new Document(fileType: FileType.fromContentType('application/pdf'))

        then:
        doc.fileType == FileType.PDF
    }
}
