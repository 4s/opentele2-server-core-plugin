package org.opentele.server.core.model.graph

import spock.lang.Specification

class DirectedGraphSpec extends Specification {

    def "should be able to detect cycle in graph"() {
        given:
        def graph = new DirectedGraph()
        graph.addVertex("node1")
        graph.addVertex("node2")
        graph.addVertex("node3")
        graph.addVertex("node4")
        graph.addEdge("node1", "node2")
        graph.addEdge("node2", "node3")
        graph.addEdge("node3", "node2")
        graph.addEdge("node3", "node4")

        when:
        def hasCycle = graph.hasCycle()

        then:
        hasCycle
    }

    def "should be able to detect cycle in graph even with different order of adding edges"() {
        given:
        def graph = new DirectedGraph()
        graph.addVertex("node1")
        graph.addVertex("node2")
        graph.addVertex("node3")
        graph.addVertex("node4")
        graph.addEdge("node1", "node2")
        graph.addEdge("node2", "node3")
        graph.addEdge("node3", "node4")
        graph.addEdge("node3", "node2")

        when:
        def hasCycle = graph.hasCycle()

        then:
        hasCycle
    }

    def "graph without cycles should not be marked as with cycles"() {
        given:
        def graph = new DirectedGraph()
        graph.addVertex("node1")
        graph.addVertex("node2")
        graph.addVertex("node3")
        graph.addVertex("node4")
        graph.addEdge("node1", "node2")
        graph.addEdge("node2", "node3")
        graph.addEdge("node3", "node4")

        when:
        def hasCycle = graph.hasCycle()

        then:
        !hasCycle
    }

    def "graph with two forward connections to same node should not be marked as with cycles"() {
        given:
        def graph = new DirectedGraph()
        graph.addVertex("node1")
        graph.addVertex("node2")
        graph.addVertex("node3")
        graph.addVertex("node4")
        graph.addVertex("node5")
        graph.addEdge("node1", "node2")
        graph.addEdge("node2", "node3")
        graph.addEdge("node3", "node4")
        graph.addEdge("node2", "node4")
        graph.addEdge("node4", "node5")

        when:
        def hasCycle = graph.hasCycle()

        then:
        !hasCycle
    }
}
