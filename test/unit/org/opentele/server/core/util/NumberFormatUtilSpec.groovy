package org.opentele.server.core.util

import grails.buildtestdata.mixin.Build
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.model.Measurement
import org.opentele.server.model.MeasurementType
import spock.lang.Specification
import spock.lang.Unroll

@Build([Measurement, MeasurementType])
class NumberFormatUtilSpec extends Specification {

//    def setup() {
//        Locale.setDefault(Locale.forLanguageTag("da-DK"))
//    }

    @Unroll
    def "can parse"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.parse(value).doubleValue() == result

        where:
        locale     | value   | result
        "en-US"    | '37.5'  | 37.5
        "da-DK"    | '37,5'  | 37.5
	}

    @Unroll
    def "can format floats"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatFloat(value) == result

        where:
        locale     | value   | result
        "en-US"    | 37.5f   | '37.5'
        "da-DK"    | 37.5f   | '37,5'
        "en-US"    | 37f     | '37'
        "en-US"    | 37.45f  | '37.45'
        "da-DK"    | 37.45f  | '37,45'
        "en-US"    | 0f      | '0'
        "da-DK"    | 0f      | '0'
    }

    @Unroll
    def "can format floats to one decimal representation"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatFloatOneDecimal(value) == result

        where:
        locale     | value   | result
        "en-US"    | 37.5f   | '37.5'
        "da-DK"    | 37.5f   | '37,5'
        "en-US"    | 37f     | '37.0'
        "en-US"    | 37.45f  | '37.5'
        "da-DK"    | 37.45f  | '37,5'
        "en-US"    | 0f      | '0.0'
        "da-DK"    | 0.0f    | '0,0'
    }

    @Unroll
    def "can format doubles"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatDouble(value) == result

        where:
        locale     | value   | result
        "en-US"    | 3       | '3'
        "en-US"    | 123000  | '123000'
        "en-US"    | 10.5    | '10.5'
        "en-US"    | 0.0     | '0'
    }

    @Unroll
    def "can format weight"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatWeight(value) == result

        where:
        locale     | value   | result
        "da-DK"    | 3.0     | '3,0'
        "da-DK"    | 123000.1| '123000,1'
        "en-US"    | 123000.1| '123000.1'
        "da-DK"    | 10.5    | '10,5'
        "da-DK"    | 0.0     | '0,0'
        "en-US"    | 0.0     | '0.0'
    }

    @Unroll
    def "can format pulse"() {

        setup:
        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatPulse(value) == result

        where:
        locale     | value   | result
        "da-DK"    | 3.0     | '3'
        "da-DK"    | 123000.1| '123000'
        "en-US"    | 123000.1| '123000'
        "da-DK"    | 0.0     | '0'
        "en-US"    | 0.0     | '0'
    }

    @Unroll
    def "can format measurement"() {

        setup:
        MeasurementType type = new MeasurementType(typeName)
        Measurement m = Measurement.build(value: value, systolic: syst, diastolic: diast, measurementType: type)

        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.format(m) == result

        where:
        locale     | value | syst   | diast    | result    | typeName
        "da-DK"    | null  |120     | 90       | '120/90'  | MeasurementTypeName.BLOOD_PRESSURE
        "en-US"    | null  |120     | 90       | '120/90'  | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | null  |0.0     | 0.0      | '0/0'     | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | null  |0.0     | 0.0      | '0/0'     | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | 90.0  |null    | null     | '90,0'    | MeasurementTypeName.WEIGHT
        "en-US"    | 90.0  |null    | null     | '90.0'    | MeasurementTypeName.WEIGHT
        "da-DK"    | 98.0  |null    | null     | '98'      | MeasurementTypeName.SATURATION
        "en-US"    | 98.0  |null    | null     | '98'      | MeasurementTypeName.SATURATION
        "da-DK"    | 98.0  |null    | null     | '98'      | MeasurementTypeName.PULSE
        "en-US"    | 98.0  |null    | null     | '98'      | MeasurementTypeName.PULSE
    }

    @Unroll
    def "can format measurement result"() {

        setup:
        MeasurementType type = new MeasurementType(typeName)
        MeasurementResult measurement = new MeasurementResult(value: value, type: type)

        Locale.setDefault(Locale.forLanguageTag(locale))

        expect:
        NumberFormatUtil.formatMeasurementResult(measurement, typeName) == result

        where:
        locale     | value    | result    | typeName
        "da-DK"    | "120,90" | '120/90'  | MeasurementTypeName.BLOOD_PRESSURE
        "en-US"    | "120,90" | '120/90'  | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | "0,0"    | '0/0'     | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | "0,1"    | '0/1'     | MeasurementTypeName.BLOOD_PRESSURE
        "da-DK"    | 90.0     | '90,0'    | MeasurementTypeName.WEIGHT
        "en-US"    | 90.0     | '90.0'    | MeasurementTypeName.WEIGHT
        "da-DK"    | 98.0     | '98'      | MeasurementTypeName.SATURATION
        "en-US"    | 98.0     | '98'      | MeasurementTypeName.SATURATION
        "da-DK"    | 98.0     | '98'      | MeasurementTypeName.PULSE
        "en-US"    | 98.0     | '98'      | MeasurementTypeName.PULSE
    }
}
