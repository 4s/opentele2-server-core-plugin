package org.opentele.builders

import org.opentele.server.model.patientquestionnaire.PatientBooleanNode
import org.opentele.server.model.patientquestionnaire.PatientQuestionnaire
import org.opentele.server.model.questionnaire.BooleanNode
import org.opentele.server.model.questionnaire.Questionnaire
import org.opentele.server.model.questionnaire.QuestionnaireHeader

class QuestionnaireHeaderBuilder {
    def String name
    def withDraft = false
    def withPublished = true
    def revision = "1.0"

    QuestionnaireHeaderBuilder forName(String name) {
        this.name = name

        this
    }

    QuestionnaireHeaderBuilder withDraft() {
        withDraft = true
        this
    }

    QuestionnaireHeaderBuilder withPublished() {
        withPublished = true
        this
    }

    QuestionnaireHeaderBuilder withOutPublished() {
        withPublished = false
        this
    }

    QuestionnaireHeaderBuilder withRevision(version) {
        revision = version.toString()
        this
    }

    QuestionnaireHeader build() {

        QuestionnaireHeader questionnaireHeader = QuestionnaireHeader.build(name: "TestQuestionnaireHeader"+System.nanoTime())
        if(name) {
            questionnaireHeader.name = this.name
        }

        def startNode = BooleanNode.build()
        startNode.save(failOnError: true)

        Questionnaire questionnaire = Questionnaire.build(questionnaireHeader: questionnaireHeader, startNode: startNode, revision: revision)
        questionnaire.addToPatientQuestionnaires(PatientQuestionnaire.build(templateQuestionnaire: questionnaire))
        questionnaireHeader.addToQuestionnaires(questionnaire)

        if (withPublished) {
            questionnaireHeader.setActiveQuestionnaire(questionnaire)
        }

        if (withDraft) {
            Questionnaire draft = Questionnaire.build(questionnaireHeader: questionnaireHeader, startNode: BooleanNode.build().save(failOnError: true))
            questionnaire.addToPatientQuestionnaires(PatientQuestionnaire.build(templateQuestionnaire: draft))
            questionnaireHeader.addToQuestionnaires(draft)

            questionnaireHeader.setDraftQuestionnaire(draft)
        }

        questionnaireHeader.save(failOnError:true)

        questionnaireHeader
    }
}
