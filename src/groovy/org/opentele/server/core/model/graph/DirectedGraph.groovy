package org.opentele.server.core.model.graph

public class DirectedGraph {
    private Map<String, Vertex> allVertices = [:]

    public Set<Vertex> vertices() {
        return allVertices.values().toSet()
    }

    public addVertex(String name) {
        if (allVertices.containsKey(name)) {
            return
        }

        allVertices.put(name, new Vertex(name))
    }

    public void addEdge(String sourceName, String destName) {
        allVertices[sourceName].outEdges << new Edge(allVertices[destName])
    }

    public long size() {
        return allVertices.size()
    }

    // Credit for cycle detection goes here: https://tekjutsu.wordpress.com/2010/02/03/3/
    public boolean hasCycle() {
        if (size() == 0) {
            return false
        }

        Collection<Vertex> vertexCollect = vertices()
        Queue<Vertex> q // Queue will store vertices that have in-degree of zero
        long counter

        /* Calculate the in-degree of all vertices and store on the Vertex */
        for (Vertex v : vertexCollect) {
            v.inDegree = 0;
        }
        for (Vertex v : vertexCollect) {
            for (Edge edge : v.outEdges) {
                edge.destination.inDegree++
            }
        }

        /* Find all vertices with in-degree == 0 and put in queue */
        q = new LinkedList<Vertex>()
        for (Vertex v : vertexCollect) {
            if (v.inDegree == 0) {
                q.offer(v)
            }
        }
        if (q.size() == 0) {
            return true // Cycle found – all vertices have in-bound edges
        }

        /* Traverse the queue counting Vertices visited */
        for (counter = 0; !q.isEmpty(); counter++) {
            Vertex v = q.poll();
            for (Edge e : v.outEdges) {
                Vertex w = e.destination;
                if (--w.inDegree == 0) {
                    q.offer(w)
                }
            }
        }
        if (counter != vertexCollect.size()) {
            return true  //Cycle found
        }

        return false
    }
}
