package org.opentele.server.core.model

public enum ConferenceMeasurementDraftType {
    BLOOD_PRESSURE,
    LUNG_FUNCTION,
    SATURATION,
    WEIGHT,
    BLOOD_SUGAR,
    URINE_COMBI
}