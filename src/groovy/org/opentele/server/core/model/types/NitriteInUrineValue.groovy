package org.opentele.server.core.model.types

enum NitriteInUrineValue {

    // In ascending order, so that ordinal() can be used for comparing
    NEGATIVE('Neg.'),
    POSITIVE('Pos.')

	private final String value

    NitriteInUrineValue(String value) {
		this.value = value
	}

    String value() {
		value
	}

	String toString() {
        value
	}

    static boolean hasOrdinal(def ordinal) {
        return ordinal instanceof Integer ? values().any { it.ordinal() == ordinal } : false
    }

    static NitriteInUrineValue fromOrdinal(def ordinal) {
        values().find { it.ordinal() == ordinal }
    }

    public static NitriteInUrineValue fromString(String text) {
        NitriteInUrineValue result = values().find { it.value.equalsIgnoreCase(text) }
        if (!result) {
            throw new IllegalArgumentException("No enum value of NitriteInUrine with text '${text}'");
        }
        result
    }
}
