package org.opentele.server.core.model.types

enum LeukocytesInUrineValue {

    // In ascending order, so that ordinal() can be used for comparing
	NEGATIVE('Neg.'),
    PLUS_ONE('+1'),
    PLUS_TWO('+2'),
    PLUS_THREE('+3')

	private final String value

    LeukocytesInUrineValue(String value) {
		this.value = value
	}

    String value() {
		value
	}

	String toString() {
        value
	}

    static boolean hasOrdinal(def ordinal) {
        return ordinal instanceof Integer ? values().any { it.ordinal() == ordinal } : false
    }

    static LeukocytesInUrineValue fromOrdinal(def ordinal) {
        values().find { it.ordinal() == ordinal }
    }

    public static LeukocytesInUrineValue fromString(String text) {
        LeukocytesInUrineValue result = values().find { it.value.equalsIgnoreCase(text) }
        if (!result) {
            throw new IllegalArgumentException("No enum value of LeukocytesInUrine with text '${text}'");
        }
        result
    }
}
