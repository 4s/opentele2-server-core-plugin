package org.opentele.server.core.util


import java.beans.PropertyEditorSupport
import org.springframework.util.StringUtils

import java.text.DecimalFormatSymbols

class CustomFloatEditor extends PropertyEditorSupport {

    private final boolean allowEmpty

    CustomFloatEditor(boolean allowEmpty) {
        this.allowEmpty = allowEmpty
    }
    
    @Override
    void setAsText(String text) {
        
        if (this.allowEmpty && !StringUtils.hasText(text)) {
            // Treat empty String as null value.
            setValue(null);
        } else {
            setValue(parse(text))
        }
    }
    
    @Override
    String getAsText() {
        Float floatVal = (Float) getValue()
        "${floatVal}"
    }

    Float parse(String text) {

        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault())

        if (text) {
            if (text.indexOf(dfs.getDecimalSeparator()) > -1) {
                throw new IllegalArgumentException("Only ${dfs.getDecimalSeparator()} is allowed as decimal-delimiter for locale ${Locale.getDefault()}")
            }
            try {
                // Hack for now..
                
                NumberFormatUtil df = new NumberFormatUtil()
                return (Float) df.parse(text)
            } catch(Exception exception){
                throw new IllegalArgumentException("${text} is not a decimal. Please add a decimal.")
            }
        } else {
            return null
        }
    }
}