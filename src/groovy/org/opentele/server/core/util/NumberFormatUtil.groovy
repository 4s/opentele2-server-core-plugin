package org.opentele.server.core.util

import org.opentele.server.core.model.types.MeasurementTypeNameVisitor

import java.text.DecimalFormat
import org.opentele.server.model.Measurement
import org.opentele.server.core.model.types.MeasurementTypeName

class NumberFormatUtil {

    private static class MeasurementResultFormatter implements MeasurementTypeNameVisitor {
        String result
        MeasurementResult measurement

        MeasurementResultFormatter(MeasurementResult measurement) {
            this.measurement = measurement
        }

        @Override
        void visitBloodPressure() {

            def (systolic, diastolic, pulse) = (measurement.value as String).split(",") as List

            def systolicString = formatBloodpressure(systolic as Double)
            def diastolicString = formatBloodpressure(diastolic as Double)

            if (pulse) {
                def pulseString = formatPulse(pulse as Double)

                result = "${systolicString}/${diastolicString}, ${pulseString}"
            } else {
                result = "${systolicString}/${diastolicString}"
            }
        }

        @Override
        void visitCtg() {
            result = (measurement?.exported ? 'CTG.' : 'CTG')
        }

        @Override
        void visitTemperature() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitUrine() {
            result = measurement.value
        }

        @Override
        void visitUrineCombi() {
            result = measurement.value
        }

        @Override
        void visitUrineGlucose() {
            result = measurement.value
        }

        @Override
        void visitUrineBlood() {
            result = measurement.value
        }

        @Override
        void visitUrineNitrite() {
            result = measurement.value
        }

        @Override
        void visitUrineLeukocytes() {
            result = measurement.value
        }

        @Override
        void visitPulse() {
            result = formatPulse(measurement.value)
        }

        @Override
        void visitWeight() {

            result = formatWeight(measurement.value)
        }

        @Override
        void visitHemoglobin() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitSaturation() {
            def (sat, pulse) = (measurement.value as String).split(",") as List
            if(pulse) {
                result = formatSaturation(sat as Double) + "/" + formatPulse(pulse as Double)
            } else {
                result = formatSaturation(sat as Double)
            }
        }

        @Override
        void visitCrp() {
            result = measurement.value
        }

        @Override
        void visitBloodSugar() {
            result = (measurement.value as String).split(",").collect{formatDouble(it as Double)}.join(" ")
        }

        @Override
        void visitLungFunction() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitContinuousBloodSugarMeasurement() {
            result = 'CGM'
        }
    }

    private static class MeasurementFormatter implements MeasurementTypeNameVisitor {
        String result
        Measurement measurement

        MeasurementFormatter(Measurement measurement) {
            this.measurement = measurement
        }

        @Override
        void visitBloodPressure() {
            def systolic = formatBloodpressure(measurement.systolic)
            def diastolic = formatBloodpressure(measurement.diastolic)
            result = "${systolic}/${diastolic}"
        }

        @Override
        void visitCtg() {
            result = measurement.fhr.replaceAll("\\[","").replaceAll("\\]","").tokenize(",").toList().size().toString()
        }

        @Override
        void visitTemperature() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitUrine() {
            result = measurement.protein?.value()
        }

        @Override
        void visitUrineCombi() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitUrineGlucose() {
            result = measurement.glucoseInUrine?.value()
        }

        @Override
        void visitUrineBlood() {
            result = measurement.bloodInUrine?.value()
        }

        @Override
        void visitUrineNitrite() {
            result = measurement.nitriteInUrine?.value()
        }

        @Override
        void visitUrineLeukocytes() {
            result = measurement.leukocytesInUrine?.value()
        }

        @Override
        void visitPulse() {
           result = formatPulse(measurement.value)
        }

        @Override
        void visitWeight() {
            result = formatWeight(measurement.value)
        }

        @Override
        void visitHemoglobin() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitSaturation() {
            result = formatSaturation(measurement.value)
        }

        @Override
        void visitCrp() {
            result = measurement.value as Integer
        }

        @Override
        void visitBloodSugar() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitLungFunction() {
            result = formatDouble(measurement.value)
        }

        @Override
        void visitContinuousBloodSugarMeasurement() {
            // Not applicable to continuous blood sugar measurements
            result = ''
        }
    }

    public static Number parse(String string) {
        DecimalFormat decimalFormat = localeSpecificDecimalFormat()
        decimalFormat.setGroupingUsed(false)
        decimalFormat.parse(string)
    }

    public static DecimalFormat localeSpecificDecimalFormat() {
        DecimalFormat.getInstance(Locale.getDefault())
    }

    public static String formatWeight(def value) {
        String.format(Locale.getDefault(), "%.1f", value)
    }

    public static String formatSaturation(def value) {
        String.format(Locale.getDefault(), "%.0f", value)
    }

    public static String formatPulse(def pulse) {
        String.format(Locale.getDefault(), "%.0f", pulse)
    }

    public static String formatBloodpressure(def pressure) {
        String.format(Locale.getDefault(), "%.0f", pressure as Double)
    }

    public static String formatFloat(Float f) {
        f == null ? null : formatNumber(f)
    }

    public static String formatFloatOneDecimal(Float f) {
        String.format(Locale.getDefault(), "%.1f", f)
    }

    public static String formatDouble(Double d) {
        d == null ? null : formatNumber(d)
    }

    public static String format(Measurement measurement) {
        MeasurementFormatter formatter = new MeasurementFormatter(measurement)
        measurement.measurementType.name.visit(formatter)
        formatter.result
    }

    public static String formatMeasurementResult(MeasurementResult measurement, MeasurementTypeName type) {
        MeasurementResultFormatter formatter = new MeasurementResultFormatter(measurement)
        type.visit(formatter)
        formatter.result
    }

    public static String formatNumber(def input) {
        DecimalFormat decimalFormat = DecimalFormat.getInstance(Locale.getDefault())
        decimalFormat.setGroupingUsed(false)
        decimalFormat.format(input)
    }
}

public class MeasurementResult {
    def value
    def type
    def unit
    def severity

    def exported // CTG

    def ignored
    def ignoredReason
    def ignoredBy

    def id //NodeResult
    def cqId //CompletedQuestionnaire
    def tqnId //TemplateQuestionnaireNode
    def pqnId //PatientQuestionnaireNode
}

