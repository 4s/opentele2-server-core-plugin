package org.opentele.server.provider.util

import org.opentele.server.core.model.types.BloodInUrineValue
import org.opentele.server.core.model.types.LeukocytesInUrineValue
import org.opentele.server.core.model.types.NitriteInUrineValue
import org.opentele.server.model.BloodPressureThreshold
import org.opentele.server.model.Measurement
import org.opentele.server.model.NumericThreshold
import org.opentele.server.model.UrineBloodThreshold
import org.opentele.server.model.UrineGlucoseThreshold
import org.opentele.server.model.UrineLeukocytesThreshold
import org.opentele.server.model.UrineNitriteThreshold
import org.opentele.server.model.UrineThreshold
import org.opentele.server.core.model.types.GlucoseInUrineValue
import org.opentele.server.core.model.types.ProteinValue

class AboveAlertThresholdPredicate extends ThresholdPredicate {
    static boolean isTrueFor(Measurement measurement) {
        def visitor = new AboveAlertThresholdPredicate(patient: measurement.patient, measurement: measurement)
        measurement.measurementType.name.visit(visitor)
        visitor.result
    }

    @Override
    Float high(NumericThreshold threshold) {
        threshold.alertHigh
    }

    @Override
    Float low(NumericThreshold threshold) {
        threshold.alertLow
    }

    @Override
    ProteinValue high(UrineThreshold threshold) {
        threshold.alertHigh
    }

    @Override
    ProteinValue low(UrineThreshold threshold) {
        threshold.alertLow
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    GlucoseInUrineValue high(UrineGlucoseThreshold threshold) {
        threshold.alertHigh
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    GlucoseInUrineValue low(UrineGlucoseThreshold threshold) {
        threshold.alertLow
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    BloodInUrineValue high(UrineBloodThreshold threshold) {
        threshold.alertHigh
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    BloodInUrineValue low(UrineBloodThreshold threshold) {
        threshold.alertLow
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    NitriteInUrineValue high(UrineNitriteThreshold threshold) {
        threshold.alertHigh
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    NitriteInUrineValue low(UrineNitriteThreshold threshold) {
        threshold.alertLow
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    LeukocytesInUrineValue high(UrineLeukocytesThreshold threshold) {
        threshold.alertHigh
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    LeukocytesInUrineValue low(UrineLeukocytesThreshold threshold) {
        threshold.alertLow
    }

    @Override
    List<Float> high(BloodPressureThreshold threshold) {
        [threshold.systolicAlertHigh, threshold.diastolicAlertHigh]
    }

    @Override
    List<Float> low(BloodPressureThreshold threshold) {
        [threshold.systolicAlertLow, threshold.diastolicAlertLow]
    }

    @Override
    void visitUrineCombi() {
        // TODO KM: Har ingen ide om hvad der skal stå her....
    }
}
