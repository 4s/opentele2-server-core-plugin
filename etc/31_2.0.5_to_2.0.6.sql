IF (object_id('documents', 'U') is null) BEGIN
  CREATE TABLE [dbo].[documents] (
    [id] BIGINT IDENTITY NOT NULL,
    [file_type] NVARCHAR(128) NOT NULL,
    [category] NVARCHAR(128) NOT NULL,
    [content] VARBINARY(MAX) NOT NULL,
    [upload_date] DATETIME2(7) NOT NULL,
    [is_read] BIT NOT NULL,
    [filename] NVARCHAR(128) NOT NULL,
    [patient_id] BIGINT NOT NULL,
    [version] BIGINT NOT NULL,
    [created_by] NVARCHAR(1024),
    [created_date] datetime2(7),
    [modified_by] NVARCHAR(1024),
    [modified_date] datetime2(7),
    CONSTRAINT [documentsPK] PRIMARY KEY (id),
    CONSTRAINT [documents_patient_FK] FOREIGN KEY (patient_id) REFERENCES [dbo].[patient] (id)
  );
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[documents]') AND name = N'documents_patient_id_idx')
  DROP INDEX [documents_patient_id_idx] ON [dbo].[documents] WITH ( ONLINE = OFF )
GO
CREATE INDEX [documents_patient_id_idx] ON [dbo].[documents]([patient_id]);
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[documents]') AND name = N'documents_upload_date_idx')
  DROP INDEX [documents_upload_date_idx] ON [dbo].[documents] WITH ( ONLINE = OFF )
GO
CREATE INDEX [documents_upload_date_idx] ON [dbo].[documents]([upload_date]);
GO