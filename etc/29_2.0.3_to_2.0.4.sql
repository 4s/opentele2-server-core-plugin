--
-- clinician2patient_group
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[clinician2patient_group]') AND name = N'clinician_id_idx')
DROP INDEX [clinician_id_idx] ON [dbo].[clinician2patient_group] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [clinician_id_idx] ON [dbo].[clinician2patient_group]
(
	[clinician_id] ASC
)
GO


--
-- message
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[message]') AND name = N'patient_id_idx')
  DROP INDEX [patient_id_idx] ON [dbo].[message] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [patient_id_idx] ON [dbo].[message]
(
  [patient_id] ASC
)
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[message]') AND name = N'patient_id_is_read_idx')
  DROP INDEX [patient_id_is_read_idx] ON [dbo].[message] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [patient_id_is_read_idx] ON [dbo].[message]
(
  [patient_id] ASC,
  [is_read]
)
GO


--
-- patient
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[patient]') AND name = N'user_id_idx')
  DROP INDEX [user_id_idx] ON [dbo].[patient] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [user_id_idx] ON [dbo].[patient]
(
  [user_id] ASC
)
GO


--
-- patient_overview
--
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'PATIENT_OVERVIEW' AND TABLE_SCHEMA ='dbo')
BEGIN
  ALTER TABLE [dbo].[PATIENT_OVERVIEW]
  ADD CONSTRAINT [patient_overview_PK]
  PRIMARY KEY CLUSTERED
    (
      [ID] ASC
    )
END

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PATIENT_OVERVIEW]') AND name = N'id_version_idx')
  DROP INDEX [id_version_idx] ON [dbo].[PATIENT_OVERVIEW] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [id_version_idx] ON [dbo].[PATIENT_OVERVIEW]
(
  [ID] ASC,
  [VERSION] ASC
)
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PATIENT_OVERVIEW]') AND name = N'important_idx')
  DROP INDEX [important_idx] ON [dbo].[PATIENT_OVERVIEW] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [important_idx] ON [dbo].[PATIENT_OVERVIEW]
(
  [IMPORTANT] ASC
)
INCLUDE ([ID], [PATIENT_ID])
GO


--
-- audit_log_parameter
--
-- check for old index existing in some databases
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[audit_log_parameter]') AND name = N'FK4D351F0AF5DB4508')
  DROP INDEX [FK4D351F0AF5DB4508] ON [dbo].[audit_log_parameter] WITH ( ONLINE = OFF )
GO
-- check for new index
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[audit_log_parameter]') AND name = N'entry_id_idx')
  DROP INDEX [entry_id_idx] ON [dbo].[audit_log_parameter] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [entry_id_idx] ON [dbo].[audit_log_parameter]
(
  [entry_id] ASC
)
GO

--
-- patient_note
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[patient_note]') AND name = N'reminder_date_idx')
  DROP INDEX [reminder_date_idx] ON [dbo].[patient_note] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [reminder_date_idx] ON [dbo].[patient_note]
(
  [reminder_date] ASC
)
INCLUDE ([id], [patient_id])
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[patient_note]') AND name = N'patient_id_idx')
  DROP INDEX [patient_id_idx] ON [dbo].[patient_note] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [patient_id_idx] ON [dbo].[patient_note]
(
  [patient_id] ASC
)
INCLUDE ([id], [version], [created_by], [created_date], [modified_by], [modified_date], [note], [reminder_date], [type])
GO


--
-- completed_questionnaire
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[completed_questionnaire]') AND name = N'patient_id_idx')
  DROP INDEX [patient_id_idx] ON [dbo].[completed_questionnaire] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [patient_id_idx] ON [dbo].[completed_questionnaire]
(
  [patient_id] ASC
)
INCLUDE ([upload_date])
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[completed_questionnaire]') AND name = N'patient_id_show_acknowledgement_to_patient_acknowledged_date_idx')
  DROP INDEX [patient_id_show_acknowledgement_to_patient_acknowledged_date_idx] ON [dbo].[completed_questionnaire] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [patient_id_show_acknowledgement_to_patient_acknowledged_date_idx] ON [dbo].[completed_questionnaire]
(
  [patient_id] ASC,
  [show_acknowledgement_to_patient] ASC,
  [acknowledged_date] ASC
)
INCLUDE ([id], [version], [_questionnaire_ignored], [acknowledged_by_id], [acknowledged_note], [created_by], [created_date], [modified_by], [modified_date], [patient_questionnaire_id], [questionnaire_ignored_reason], [questionnare_ignored_by_id], [severity], [upload_date], [questionnaire_header_id], [received_date])
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[completed_questionnaire]') AND name = N'acknowledged_date_patient_id_idx')
  DROP INDEX [acknowledged_date_patient_id_idx] ON [dbo].[completed_questionnaire] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [acknowledged_date_patient_id_idx] ON [dbo].[completed_questionnaire]
(
  [patient_id] ASC,
  [acknowledged_date] ASC
)
GO


--
-- questionnaire_schedule
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[questionnaire_schedule]') AND name = N'monitoring_plan_id_idx')
  DROP INDEX [monitoring_plan_id_idx] ON [dbo].[questionnaire_schedule] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [monitoring_plan_id_idx] ON [dbo].[questionnaire_schedule]
(
  [monitoring_plan_id] ASC
)
GO


--
-- measurement
--
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[measurement]') AND name = N'measurement_node_result_id_idx')
  DROP INDEX [measurement_node_result_id_idx] ON [dbo].[measurement] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [measurement_node_result_id_idx] ON [dbo].[measurement]
(
  [measurement_node_result_id] ASC
)
INCLUDE ([id], [diastolic], [exported], [glucose_in_urine], [measurement_type_id], [protein], [systolic], [unit], [value])
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[measurement]') AND name = N'measurement_type_id_patient_id_time_idx')
  DROP INDEX [measurement_type_id_patient_id_time_idx] ON [dbo].[measurement] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [measurement_type_id_patient_id_time_idx] ON [dbo].[measurement]
(
  [measurement_type_id],
  [patient_id],
  [time]
)
INCLUDE ([id], [version], [created_by], [created_date], [diastolic], [end_time], [exported], [fetal_height], [fhr], [glucose_in_urine], [has_temperature_warning], [is_after_meal], [is_before_meal], [is_control_measurement], [is_out_of_bounds], [mean_arterial_pressure], [measurement_node_result_id], [meter_id], [mhr], [modified_by], [modified_date], [other_information], [protein], [qfhr], [signal_to_noise], [signals], [start_time], [systolic], [toco], [unit], [unread], [value], [voltage_end], [voltage_start], [fev6], [fev1fev6ratio], [fef2575], [is_good_test], [fev_software_version], [exported_to_kih], [conference_id], [device_identification], [kih_uuid], [kih_uuid_systolic], [kih_uuid_diastolic], [consultation_id])
GO


---
------ CREATE TEXT_MESSAGE_TEMPLATE TABLE
---
CREATE TABLE [dbo].[text_message_template] (
  [id] BIGINT IDENTITY NOT NULL,
  [name] NVARCHAR(255) NOT NULL,
  [content] NVARCHAR(2048) NOT NULL,
  [version] BIGINT NOT NULL,
  [created_by] NVARCHAR(1024),
  [created_date] datetime2(7),
  [modified_by] NVARCHAR(1024),
  [modified_date] datetime2(7),
  CONSTRAINT [text_message_templatePK] PRIMARY KEY (id)
)
GO
