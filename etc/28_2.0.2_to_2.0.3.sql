ALTER TABLE PATIENT ALTER COLUMN CPR nvarchar(128) NOT NULL
GO


DROP INDEX [patient_cpr_idx] ON [dbo].[audit_log_entry]
GO
ALTER TABLE AUDIT_LOG_ENTRY ALTER COLUMN patient_cpr nvarchar(128)
GO
CREATE INDEX [patient_cpr_idx] ON [dbo].[audit_log_entry]([patient_cpr])
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[monitor_kit]') AND name = N'UQ__monitor___750D36D24959E263')
ALTER TABLE [dbo].[monitor_kit] DROP CONSTRAINT [UQ__monitor___750D36D24959E263]
GO

DROP INDEX [monkit_deptid_name_unique] ON [dbo].[monitor_kit]
GO
ALTER TABLE MONITOR_KIT ALTER COLUMN name nvarchar(128)
GO
CREATE INDEX [monkit_deptid_name_unique] ON [dbo].[monitor_kit]([department_id], [name])
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[patient_group]') AND name = N'UQ__patient___750D36D2725BF7F6')
ALTER TABLE [dbo].[patient_group] DROP CONSTRAINT [UQ__patient___750D36D2725BF7F6]
GO

DROP INDEX [ptgroup_deptid_name_unique] ON [dbo].[patient_group]
GO
ALTER TABLE PATIENT_GROUP ALTER COLUMN name nvarchar(128)
GO
CREATE INDEX [ptgroup_deptid_name_unique] ON [dbo].[patient_group]([department_id], [name])
GO

TRUNCATE TABLE real_timectg;
GO

EXEC sp_rename 'real_timectg', 'real_time_ctg';
GO

ALTER TABLE [dbo].[real_time_ctg] ADD [patient_id] BIGINT NOT NULL
GO

ALTER TABLE [dbo].[real_time_ctg]
  ADD CONSTRAINT [real_time_ctg_patient_FK]
  FOREIGN KEY ([patient_id])
  REFERENCES [dbo].[patient] ([id])
GO

CREATE INDEX [patient_id_created_date_idx] ON [dbo].[real_time_ctg]([patient_id], [created_date]);
GO
