databaseChangeLog = {
    changeSet(author: "RA (kih-1903)", id: "kih-1903-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "patient", columnName: "place")
            }
        }

        addColumn(tableName: 'patient') {
            column(name: 'place', type: '${string.type}')
        }
    }
}