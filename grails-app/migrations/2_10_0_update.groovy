databaseChangeLog = {
    changeSet(author: "EMH (kih-1266)", id: "kih-1296-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "patient_questionnaire_node", columnName: "monica_uterine_activity_sensitivity_high")
            }
        }

        addColumn(tableName: 'patient_questionnaire_node') {
            column(name: 'monica_uterine_activity_sensitivity_high', type: '${boolean.type}')
        }
        addColumn(tableName: 'questionnaire_node') {
            column(name: 'monica_uterine_activity_sensitivity_high', type: '${boolean.type}')
        }
    }
}