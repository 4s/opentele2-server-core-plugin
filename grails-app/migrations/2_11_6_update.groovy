databaseChangeLog = {

    changeSet(author: "RA (kih-1956)", id: "kih-1956-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "patient", columnName: "date_of_birth")
            }
        }

        addColumn(tableName: 'patient') {
            column(name: 'date_of_birth', type: '${datetime.type}')
        }
    }

    changeSet(author: "RA (OPENTELE-82)", id: "OPENTELE-82-1") {
        preConditions(onFail: 'MARK_RAN') {
            columnExists(tableName: "questionnaire_node", columnName: "simulate")
            columnExists(tableName: "patient_questionnaire_node", columnName: "simulate")
        }

        dropColumn(tableName: 'questionnaire_node', columnName: 'simulate')
        dropColumn(tableName: 'patient_questionnaire_node', columnName: 'simulate')
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-1") {

        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "urine_blood_threshold")
            }
        }

        createTable(tableName: "urine_blood_threshold") {
            column(name: "id", type: '${id.type}') {
                constraints(nullable: "false",
                        primaryKey: "true",
                        primaryKeyName: "urine_blood_thresholdPK")
            }

            column(name: "alert_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "alert_low", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_low", type: '${string.type}') {
                constraints(nullable: "true")
            }
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-2") {

        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "urine_nitrite_threshold")
            }
        }

        createTable(tableName: "urine_nitrite_threshold") {
            column(name: "id", type: '${id.type}') {
                constraints(nullable: "false",
                        primaryKey: "true",
                        primaryKeyName: "urine_nitrite_thresholdPK")
            }

            column(name: "alert_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "alert_low", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_low", type: '${string.type}') {
                constraints(nullable: "true")
            }
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-3") {

        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "urine_leukocytes_threshold")
            }
        }

        createTable(tableName: "urine_leukocytes_threshold") {
            column(name: "id", type: '${id.type}') {
                constraints(nullable: "false",
                        primaryKey: "true",
                        primaryKeyName: "urine_leukocytes_thresholdPK")
            }

            column(name: "alert_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "alert_low", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_high", type: '${string.type}') {
                constraints(nullable: "true")
            }

            column(name: "warning_low", type: '${string.type}') {
                constraints(nullable: "true")
            }
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-4") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "measurement", columnName: "blood_in_urine")
            }
        }

        addColumn(tableName: 'measurement') {
            column(name: 'blood_in_urine', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-5") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "measurement", columnName: "nitrite_in_urine")
            }
        }

        addColumn(tableName: 'measurement') {
            column(name: 'nitrite_in_urine', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-6") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "measurement", columnName: "leukocytes_in_urine")
            }
        }

        addColumn(tableName: 'measurement') {
            column(name: 'leukocytes_in_urine', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-7") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "blood_sugar")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'blood_sugar', type: '${double.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-8") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "protein")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'protein', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-9") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "urine_glucose")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'urine_glucose', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-10") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "urine_blood")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'urine_blood', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-11") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "urine_nitrite")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'urine_nitrite', type: '${text.type}')
        }
    }

    changeSet(author: "PU (OPENTELE-93)", id: "OPENTELE-93-12") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                columnExists(tableName: "conference_measurement_draft", columnName: "urine_leukocytes")
            }
        }

        addColumn(tableName: 'conference_measurement_draft') {
            column(name: 'urine_leukocytes', type: '${text.type}')
        }
    }
}