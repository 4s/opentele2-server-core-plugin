databaseChangeLog = {
    changeSet(author: "RA (kih-1774)", id: "kih-1774-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                primaryKeyExists(tableName: "patient_overview")
            }
        }
        addPrimaryKey(columnNames: "id", constraintName: "patient_overview_PK", tableName: "patient_overview")
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-2") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "patient_id_idx", tableName: "patient_note")
            }
        }
        createIndex(indexName: "patient_id_idx", tableName: "patient_note") {
            column(name: "patient_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-3") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "clinician_id_idx", tableName: "clinician2patient_group")
            }
        }
        createIndex(indexName: "clinician_id_idx", tableName: "clinician2patient_group") {
            column(name: "clinician_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-4") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "patient_id_idx", tableName: "message")
            }
        }
        createIndex(indexName: "patient_id_idx", tableName: "message") {
            column(name: "patient_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-5") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "patient_id_is_read_idx", tableName: "message")
            }
        }
        createIndex(indexName: "patient_id_is_read_idx", tableName: "message") {
            column(name: "patient_id")
            column(name: "is_read")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-6") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "user_id_idx", tableName: "patient")
            }
        }
        createIndex(indexName: "user_id_idx", tableName: "patient") {
            column(name: "user_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-7") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "id_version_idx", tableName: "patient_overview")
            }
        }
        createIndex(indexName: "id_version_idx", tableName: "patient_overview") {
            column(name: "id")
            column(name: "version")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-8") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "important_idx", tableName: "patient_overview")
            }
        }
        createIndex(indexName: "important_idx", tableName: "patient_overview") {
            column(name: "important")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-9") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "important_idx", tableName: "patient_overview")
            }
        }
        createIndex(indexName: "important_idx", tableName: "patient_overview") {
            column(name: "important")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-10") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "entry_id_idx", tableName: "audit_log_parameter")
            }
        }
        createIndex(indexName: "entry_id_idx", tableName: "audit_log_parameter") {
            column(name: "entry_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-11") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "reminder_date_idx", tableName: "patient_note")
            }
        }
        createIndex(indexName: "reminder_date_idx", tableName: "patient_note") {
            column(name: "reminder_date")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-12") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "patient_id_idx", tableName: "completed_questionnaire")
            }
        }
        createIndex(indexName: "patient_id_idx", tableName: "completed_questionnaire") {
            column(name: "patient_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-13") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "patient_id_show_acknowledgement_to_patient_acknowledged_date_idx", tableName: "completed_questionnaire")
            }
        }
        createIndex(indexName: "patient_id_show_acknowledgement_to_patient_acknowledged_date_idx", tableName: "completed_questionnaire") {
            column(name: "patient_id")
            column(name: "show_acknowledgement_to_patient")
            column(name: "acknowledged_date")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-14") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "acknowledged_date_patient_id_idx", tableName: "completed_questionnaire")
            }
        }
        createIndex(indexName: "acknowledged_date_patient_id_idx", tableName: "completed_questionnaire") {
            column(name: "patient_id")
            column(name: "acknowledged_date")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-15") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "monitoring_plan_id_idx", tableName: "questionnaire_schedule")
            }
        }
        createIndex(indexName: "monitoring_plan_id_idx", tableName: "questionnaire_schedule") {
            column(name: "monitoring_plan_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-16") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "measurement_node_result_id_idx", tableName: "measurement")
            }
        }
        createIndex(indexName: "measurement_node_result_id_idx", tableName: "measurement") {
            column(name: "measurement_node_result_id")
        }
    }

    changeSet(author: "RA (kih-1774)", id: "kih-1774-17") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                indexExists(indexName: "measurement_type_id_patient_id_time_idx", tableName: "measurement")
            }
        }
        createIndex(indexName: "measurement_type_id_patient_id_time_idx", tableName: "measurement") {
            column(name: "measurement_type_id")
            column(name: "patient_id")
            column(name: "time")
        }
    }

    changeSet(author: "RA (kih-1762)", id: "kih-1762-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "text_message_template")
            }
        }
        createTable(tableName: "text_message_template") {
            column(autoIncrement: "true", name: "id", type: '${id.type}') {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "text_message_template_PK")
            }

            column(name: "name", type: '${string255.type}') {
                constraints(nullable: "false")
            }
            column(name: "content", type: '${string2048.type}') {
                constraints(nullable: "false")
            }
            column(name: "version", type: '${id.type}') { constraints(nullable: "false") }
            column(name: "created_by", type: '${string.type}')
            column(name: "created_date", type: '${datetime.type}')
            column(name: "modified_by", type: '${string.type}')
            column(name: "modified_date", type: '${datetime.type}')
        }
    }
}