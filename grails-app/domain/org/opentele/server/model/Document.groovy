package org.opentele.server.model

import org.opentele.server.core.model.AbstractObject

class Document extends AbstractObject {

    static belongsTo = [patient: Patient]

    FileType fileType = FileType.PDF
    DocumentCategory category
    byte[] content
    Date uploadDate
    boolean isRead = false
    String filename

    static constraints = {
        patient nullable: false
        category nullable: false
        fileType nullable: false
        uploadDate nullable: false
        isRead nullable: false
        filename nullable: false
        content minSize: 1
    }

    static mapping = {
        table 'documents'
    }
}


