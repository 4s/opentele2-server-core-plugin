package org.opentele.server.model

import org.opentele.server.core.model.AbstractObject

class LinksCategory extends AbstractObject {

    String name

    static hasMany = [links: Link, patientGroups: PatientGroup]

    static constraints = {
        name nullable: false, blank: false
        links validator: {val, obj, errors ->
            if (val == null || val.size() < 1) {
                errors.rejectValue('links', 'linksCategory.links.minSize.notmet')
                return false
            }
            return true
        }
        patientGroups validator: {val, obj, errors ->
            if (val == null || val.size() < 1) {
                errors.rejectValue('patientGroups', 'linksCategory.patientGroups.minSize.notmet')
                return false
            }
            return true
        }
    }

    static mapping = {
        table 'links_categories'
        links column: 'links_category_id', joinTable: false
    }

    static List<LinksCategory> byPatientGroups(groups) {
        def patientGroupIdsForPatient = groups.collect { it.id }
        return LinksCategory.withCriteria {
            patientGroups {
                inList('id', patientGroupIdsForPatient)
            }
        }
    }
}
