package org.opentele.server.model

import org.opentele.server.core.model.AbstractObject

class Link extends AbstractObject{

    String title
    String url

    static belongsTo = LinksCategory

    static constraints = {
        title(nullable: false, blank: false)
        url(nullable: false, url: true)
    }

    static mapping = {
        table 'links'
    }
}
