package org.opentele.server.model

import org.opentele.server.core.model.types.BloodInUrineValue

class UrineBloodThreshold extends Threshold {
    BloodInUrineValue alertHigh
    BloodInUrineValue warningHigh
    BloodInUrineValue warningLow
    BloodInUrineValue alertLow

    static constraints = {
        alertHigh nullable: true, validator: { value, object ->
            value == null || value > [object.warningHigh, object.warningLow, object.alertLow].max()
        }
        warningHigh nullable: true, validator: { value, object ->
            value == null || value > [object.warningLow, object.alertLow].max()
        }
        warningLow nullable: true, validator: { value, object ->
            value == null || value > object.alertLow
        }
        alertLow nullable: true
    }

    @Override
    Threshold duplicate() {
        return new UrineBloodThreshold(type: this.type,
                alertHigh: this.alertHigh, alertLow: this.alertLow,
                warningHigh: this.warningHigh, warningLow: this.warningLow)
    }

    @Override
    Map thresholdValuesToMap() {
        [
            alertHigh  : alertHigh?.value(),
            warningHigh: warningHigh?.value(),
            warningLow : warningLow?.value(),
            alertLow   : alertLow?.value()
        ]
    }

    UrineBloodThreshold fromMap(values) {
        alertHigh = parseValue(values.alertHigh)
        warningHigh = parseValue(values.warningHigh)
        warningLow = parseValue(values.warningLow)
        alertLow = parseValue(values.alertLow)

        return this
    }

    private BloodInUrineValue parseValue(value) {
        if (!value) {
            return null
        } else {
            return BloodInUrineValue.fromString(value)
        }
    }
}
