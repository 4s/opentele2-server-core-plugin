package org.opentele.server.model

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.opentele.server.core.model.AbstractObject
import org.opentele.server.core.model.ConferenceMeasurementDraftType

import java.util.regex.Pattern

class ConferenceMeasurementDraft extends AbstractObject {

    static belongsTo = [
        conference: Conference
    ]

    static Pattern WILDCARD_PATTERN = Pattern.compile('.+')
    static Pattern INTEGER_PATTERN = Pattern.compile('\\d+')
    static Pattern DOUBLE_PATTERN_ONE_DECIMAL = Pattern.compile('\\d+(,\\d{1})?')
    static Pattern DOUBLE_PATTERN_TWO_DECIMALS = Pattern.compile('\\d+(,\\d{1,2})?')
    static NUMBER_CONVERTER = {
        it == '' ? null : it.replace(',', '.') as double
    }
    static IDENTITY_CONVERTER = { it }
    static Map<String, Pattern> customValidators = [:]
    static Map<String, Closure> customConverters = [:]

    boolean included
    boolean automatic
    boolean waiting
    String deviceId

    ConferenceMeasurementDraftType getType() {
        throw new UnsupportedOperationException('Not implemented - override in subtype.')
    }

    List<String> getWarningFields() {
        throw new UnsupportedOperationException('Not implemented - override in subtype.')
    }

    List<String> getErrorFields() {
        getErrors().getFieldErrors().collect { it.field }
    }

    void updateFields(GrailsParameterMap params) {
        params.each {
            if (customValidators.containsKey(it.key)) {
                if (it.value == '' || customValidators[it.key as String].matcher(it.value as String).matches()) {
                    properties."${it.key}" = customConverters[it.key as String]it.value
                } else {
                    errors.rejectValue(it.key as String, it.value as String)
                }
            }
        }
        included = params.included
    }

    static transients = [ 'type', 'warningFields', 'errorFields' ]
    static constraints = {
        deviceId(nullable: true)
    }

}
