package org.opentele.server.model

import org.opentele.server.core.model.ConferenceMeasurementDraftType

import java.util.regex.Pattern

class ConferenceBloodSugarMeasurementDraft extends ConferenceMeasurementDraft {

    Double bloodSugar

    static constraints = {
        bloodSugar(nullable: true)
    }

    static Map<String, Pattern> customValidators = [
            'bloodSugar': DOUBLE_PATTERN_ONE_DECIMAL
    ]

    @Override
    ConferenceMeasurementDraftType getType() {
        ConferenceMeasurementDraftType.BLOOD_SUGAR
    }

    static Map<String, Closure> customConverters = [
            'bloodSugar': NUMBER_CONVERTER
    ]

    @Override
    List<String> getWarningFields() {
        []
    }

}
