package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class UrineNitriteMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public UrineNitriteMeasurementNodeOutput(node) {

        OutputVariable nitriteUrine = new OutputVariable(
                "${node.id}.${MeasurementNode.NITRITE_URINE_VAR}",
                DataType.INTEGER.value(), 'nitriteUrine')

        this.outputVariables = [nitriteUrine]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'NitriteUrineDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.NITRITE_URINE_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.NITRITE_URINE_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
