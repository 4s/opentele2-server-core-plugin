package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class ContinuousBloodSugarMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public ContinuousBloodSugarMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.CGM#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(),
                'deviceId')
        OutputVariable continuousBloodSugar = new OutputVariable(
                "${node.id}.CGM#${MeasurementNode.CONTINUOUS_BLOOD_SUGAR_MEASUREMENTS_VAR}",
                DataType.STRING_ARRAY.value(),
                'events')

        this.outputVariables = [deviceId, continuousBloodSugar]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'ContinuousBloodSugarDeviceNode'
        this.cancelVariableName = "${node.id}.CGM##CANCEL"
        this.severityVariableName = "${node.id}.CGM##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
