package org.opentele.server.core.measurement_nodes

import groovy.transform.Immutable

@Immutable
class OutputVariable {
    String name, type, key
}
