package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.QuestionnaireOutputElementBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class HaemoglobinMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public HaemoglobinMeasurementNodeOutput(node) {

        OutputVariable haemoglobin = new OutputVariable(
                "${node.id}.${MeasurementNode.HEMOGLOBIN_VAR}",
                DataType.FLOAT.value(),
                'haemoglobinValue')

        Closure mapToInputFieldsClosure = { aNode, QuestionnaireOutputBuilder outputBuilder ->
            def elements = []
            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: aNode.text)

            if (aNode.helpInfo) {
                QuestionnaireOutputElementBuilder.helpTextElement(
                        text: aNode.helpInfo?.text,
                        imageFile: aNode.helpInfo?.helpImage?.id)
            }

            elements << QuestionnaireOutputElementBuilder.editFloatElement(
                    outputBuilder.getOutputVariables(),
                    [variableName: "${node.id}.${MeasurementNode.HEMOGLOBIN_VAR}"])
            elements << outputBuilder.buttonsToSkipInputForNode(
                    leftNext: outputBuilder.cancelNodeName(aNode),
                    rightNext: outputBuilder.defaultNextSeverityNodeName(aNode))
            outputBuilder.addIoNode(nodeName: aNode.id,
                    nextNodeId: aNode.defaultNext.id,
                    elements: elements)
        }

        this.outputVariables = [haemoglobin]
        this.mapToInputFieldsClosure = mapToInputFieldsClosure
        this.customClosure = null
        this.nodeName = 'HaemoglobinDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.HEMOGLOBIN_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.HEMOGLOBIN_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
