package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.QuestionnaireOutputElementBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode
import org.springframework.context.i18n.LocaleContextHolder

class BloodPressureAndPulseMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public BloodPressureAndPulseMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.BP#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable diastolic = new OutputVariable(
                "${node.id}.BP#${MeasurementNode.DIASTOLIC_VAR}",
                DataType.INTEGER.value(), 'diastolic')

        OutputVariable systolic = new OutputVariable(
                "${node.id}.BP#${MeasurementNode.SYSTOLIC_VAR}",
                DataType.INTEGER.value(), 'systolic')

        OutputVariable meanArterialPressure = new OutputVariable(
                "${node.id}.BP#${MeasurementNode.MEAN_ARTERIAL_PRESSURE_VAR}",
                DataType.INTEGER.value(), 'meanArterialPressure')

        OutputVariable pulse = new OutputVariable(
                "${node.id}.BP#${MeasurementNode.PULSE_VAR}",
                DataType.INTEGER.value(), 'pulse')

        List variables = [
                deviceId, diastolic, systolic, meanArterialPressure, pulse
        ]

        def mapToInputFieldsClosure = { aNode, QuestionnaireOutputBuilder outputBuilder ->

            def elements = []
            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: aNode.text)

            if (aNode.helpInfo) {
                elements << QuestionnaireOutputElementBuilder
                        .helpTextElement(text: aNode.helpInfo?.text,
                        imageFile: aNode.helpInfo?.helpImage?.id)
            }

            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: outputBuilder.getMessageSource().getMessage(
                    'default.bloodpressure.systolic',
                    new String[0], LocaleContextHolder.locale))
            elements << QuestionnaireOutputElementBuilder
                    .editIntegerElement(outputBuilder.getOutputVariables(), [
                    variableName: "${node.id}.BP#${MeasurementNode.SYSTOLIC_VAR}"
            ])

            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: outputBuilder.getMessageSource().getMessage(
                    'default.bloodpressure.diastolic',
                    new String[0], LocaleContextHolder.locale))
            elements << QuestionnaireOutputElementBuilder
                    .editIntegerElement(outputBuilder.getOutputVariables(), [
                    variableName: "${node.id}.BP#${MeasurementNode.DIASTOLIC_VAR}"
            ])

            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: outputBuilder.getMessageSource().getMessage(
                    'default.pulse',
                    new String[0], LocaleContextHolder.locale))
            elements << QuestionnaireOutputElementBuilder
                    .editIntegerElement(outputBuilder.getOutputVariables(), [
                    variableName: "${node.id}.BP#${MeasurementNode.PULSE_VAR}"
            ])

            elements << outputBuilder.buttonsToSkipInputForNode(
                    leftNext: outputBuilder.cancelNodeName(aNode),
                    rightNext: outputBuilder.defaultNextSeverityNodeName(aNode))

            outputBuilder.addIoNode(nodeName: aNode.id,
                    nextNodeId: outputBuilder.defaultNextSeverityNodeName(aNode),
                    elements: elements)
        }

        this.outputVariables = variables
        this.mapToInputFieldsClosure = mapToInputFieldsClosure
        this.customClosure = null
        this.nodeName = 'BloodPressureDeviceNode'
        this.cancelVariableName = "${node.id}.BP##CANCEL"
        this.severityVariableName = "${node.id}.BP##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
