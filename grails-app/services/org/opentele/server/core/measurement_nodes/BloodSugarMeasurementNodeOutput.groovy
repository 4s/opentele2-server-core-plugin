package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class BloodSugarMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public BloodSugarMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.BS#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable bloodSugar = new OutputVariable(
                "${node.id}.BS#${MeasurementNode.BLOODSUGAR_VAR}",
                DataType.INTEGER.value(), 'bloodSugarMeasurements')

        List variables = [
                deviceId, bloodSugar
        ]

        this.outputVariables = variables
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = node.mapToInputFields ? 'BloodSugarManualDeviceNode' : 'BloodSugarDeviceNode'
        this.cancelVariableName = "${node.id}.BS##CANCEL"
        this.severityVariableName = "${node.id}.BS##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
