package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class LungMonitorMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public LungMonitorMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable fev1 = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEV1_VAR}",
                DataType.FLOAT.value(), 'fev1')

        OutputVariable fev6 = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEV6_VAR}",
                DataType.FLOAT.value(), 'fev6')

        OutputVariable fev1Fev6Ratio = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEV1_FEV6_RATIO_VAR}",
                DataType.FLOAT.value(), 'fev1Fev6Ratio')

        OutputVariable fef2575 = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEF2575_VAR}",
                DataType.FLOAT.value(), 'fef2575')

        OutputVariable goodTest = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEV_GOOD_TEST_VAR}",
                DataType.BOOLEAN.value(), 'goodTest')

        OutputVariable softwareVersion = new OutputVariable(
                "${node.id}.LF#${MeasurementNode.FEV_SOFTWARE_VERSION}",
                DataType.INTEGER.value(), 'softwareVersion')

        List variables = [
                deviceId, fev1, fev6, fev1Fev6Ratio, fef2575, goodTest, softwareVersion
        ]

        this.outputVariables = variables
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'LungMonitorDeviceNode'
        this.cancelVariableName = "${node.id}.LF##CANCEL"
        this.severityVariableName = "${node.id}.LF##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
