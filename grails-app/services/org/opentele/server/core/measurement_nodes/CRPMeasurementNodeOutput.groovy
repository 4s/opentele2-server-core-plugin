package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class CRPMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public CRPMeasurementNodeOutput(node) {

        OutputVariable cpr = new OutputVariable(
                "${node.id}.${MeasurementNode.CRP_VAR}",
                DataType.INTEGER.value(), 'CRP')

        this.outputVariables = [cpr]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'CRPNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.CRP_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.CRP_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
