package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.QuestionnaireOutputElementBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class WeightMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public WeightMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.${MeasurementNode.WEIGHT_VAR}#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable weight = new OutputVariable(
                "${node.id}.${MeasurementNode.WEIGHT_VAR}",
                DataType.FLOAT.value(), 'weight')

        Closure mapToInputFieldsClosure = { aNode, QuestionnaireOutputBuilder outputBuilder ->

            def elements = []
            elements << QuestionnaireOutputElementBuilder.textViewElement(text: aNode.text)

            if (aNode.helpInfo) {
                elements << QuestionnaireOutputElementBuilder.helpTextElement(
                        text: aNode.helpInfo?.text,
                        imageFile: aNode.helpInfo?.helpImage?.id)
            }

            elements << QuestionnaireOutputElementBuilder.editFloatElement(
                    outputBuilder.getOutputVariables(),
                    [variableName: "${node.id}.${MeasurementNode.WEIGHT_VAR}"])
            elements << outputBuilder.buttonsToSkipInputForNode(
                    leftNext: outputBuilder.cancelNodeName(aNode),
                    rightNext: outputBuilder.defaultNextSeverityNodeName(aNode))

            outputBuilder.addIoNode(nodeName: aNode.id,
                    nextNodeId: aNode.defaultNext.id,
                    elements: elements)
        }

        this.outputVariables = [deviceId, weight]
        this.mapToInputFieldsClosure = mapToInputFieldsClosure
        this.customClosure = null
        this.nodeName = 'WeightDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.WEIGHT_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.WEIGHT_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
